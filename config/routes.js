'use strict';

var express = require('express');
var path = require('path');
var qs = require('querystring');
var log = require('loglevel');

/**
 * Express configuration
 */
module.exports = function(app, io) {
	var env = app.get('env');
        
        io.sockets.on('connection', function(socket) {
          log.debug('client connected!');
        });

	// Static resources
	//app.use(express.favicon(path.join(__dirname, '../public', 'favicon.ico')));
	app.use(express.static(path.join(__dirname, '../public')));
	app.use(express.static(path.join(__dirname, '../bower_components')));

	app.get('/*', function(req, res, next) {
		if (!req.path.match(/(\.js|\.html)$/)) {
			res.sendfile('index.html', { root: path.join(__dirname, '../public') });
		} else {
			next();
		}
	});

	// Error handler
	if (env === 'development') {
		app.use(require('errorhandler')());
	}
};
