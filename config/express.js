'use strict';

var express = require('express'),
	fs = require('fs'),
	path = require('path'),
	helmet = require('helmet'),
	bodyParser = require('body-parser'),
        cors = require('cors');

/**
 * Express configuration
 */
module.exports = function(app) {
	var env = app.get('env');

	// Logging
	if (env === 'development') {
		app.use(require('morgan')('dev'));
	} else if(env == 'production') {
        // Do not log any requests
    } else if (env !== 'test') {
		app.use(require('morgan')());
	}

	// Basic request processing:
	app.use(require('cookie-parser')(process.env.COOKIE_SECRET || 'H2YlmVI=srH5DCw4xKA(IA4YZ|Gr4gutt|Lh0WD:'));

	app.use(bodyParser.json());
	app.use(require('connect-timeout')(10 * 1000));

	// Live reload
//	if (env === 'development') {
//		app.use(require('connect-livereload')());
//	}

	// Disable caching of scripts for easier testing
	if (env === 'development') {
		app.use(function noCache(req, res, next) {
			if (req.url.indexOf('/views/') === 0 || req.url.indexOf('/controllers/') === 0) {
				res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
				res.header('Pragma', 'no-cache');
				res.header('Expires', 0);
			}
			next();
		});
	}

	// Compression
	if (env === 'production') {
		app.use(require('compression')());
	}

	// Security
	app.use(helmet.xssFilter());
	app.use(helmet.nosniff());
	app.use(helmet.xframe('sameorigin'));
	app.use(helmet.hidePoweredBy());
	app.use(helmet.hsts({
		maxAge: 1000 * 60 * 60 * 24 // 1 day, increase later.
	}));
        
    app.use(cors());

	// Views with markdown support
	app.engine('html', function(file, options, callback) {
		var layoutFile = path.dirname(file) + '/_layout.html';
		fs.readFile(file, 'utf8', function(err, html) {
			if (err) return callback(err);
			fs.readFile(layoutFile, 'utf8', function(err, layout) {
				if (err) return callback(err);
				callback(err, layout.replace('{{ content }}', html))
			});
		});

	});
	app.set('views', path.join(__dirname, '../content'));

};
