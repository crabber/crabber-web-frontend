module.exports = function(grunt) {

	grunt.loadNpmTasks('grunt-ng-constant');

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		ngconstant: {
			// Options for all targets
			options: {
				space: '  ',
				wrap: '"use strict";\n\n {%= __ngModule %}',
				name: 'config',
			},

			// Environment targets
			development: {
				options: {
					dest: './public/config.js'
				},
				constants: {
					ENV: {
						name: 'development',
						apiBaseUrl: 'http://localhost:5001'
					}
				}
			},
			production: {
				options: {
					dest: './public/config.js'
				},
				constants: {
					ENV: {
						name: 'production',
						apiBaseUrl: 'https://crabber-backend.cute.systems'
					}
				}
			}
		},
	});

	grunt.registerTask('default', ['ngconstant:production']);
	grunt.registerTask('production', ['ngconstant:production']);
	grunt.registerTask('dev', ['ngconstant:development']);
};
