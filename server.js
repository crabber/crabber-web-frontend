(function() {
	'use strict';

	// Setup default environment variables
	process.env.NODE_ENV = process.env.NODE_ENV || 'production';
	process.env.PORT = process.env.PORT || 5000;

	// Load and initialize express and socket.io
	var app = require('express')();
    var server = require('http').createServer(app);
    var io = require('socket.io')(server);
    var log = require('loglevel');
    
    if(process.env.NODE_ENV === 'production') {
        log.setLevel(log.levels.INFO);
    } else {
        log.setLevel(log.levels.DEBUG);   
    }

	// Express settings
	require('./config/express')(app);

	// Routing
	require('./config/routes')(app, io);


	// Start server
	if (process.env.NODE_ENV !== 'test') {
		server.listen(process.env.PORT, function() {
			log.info('Server started on port %d in %s mode! Waiting for requests...', process.env.PORT, process.env.NODE_ENV);
		});
	}

	module.exports.app = app;
	module.exports.server = server;
	module.exports.io = io;

})();
