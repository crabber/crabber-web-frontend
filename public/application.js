angular.module('crabber', [
	// Angular JS core
	'ngAnimate',
	'ngRoute',
	'ngResource',
	'ngCookies',
	'ngSanitize',

	// Angular UI extensions
	'ui.utils',
	// 'ui.router', // TODO integrate this instead of ngRoute!
	'ui.select',
	'ui.sortable',

	// Angular UI Bootstrap integration
	'ui.bootstrap',

	// Angular Loading Bar
	'angular-loading-bar',
	
	// Angular moment
	'angularMoment',
    
    'config'
]);

angular.module('crabber').config(function($routeProvider, $locationProvider) {

	var baseUrl = '';

	$routeProvider.when(baseUrl + '/', {
		templateUrl: baseUrl + '/views/Dashboard.html',
		controller: 'DashboardController'
	});
	
	$routeProvider.when(baseUrl + '/login', {
		templateUrl: baseUrl + '/views/Login.html',
		controller: 'LoginController'
	});
        
        $routeProvider.when(baseUrl + '/logout', {
                templateUrl: baseUrl + '/views/Logout.html',
                controller: 'LogoutController'
        });

	// Bookings
	$routeProvider.when(baseUrl + '/bookings', {
		templateUrl: baseUrl + '/views/BookingList.html',
		controller: 'BookingListController'
	});
	$routeProvider.when(baseUrl + '/bookings/add', {
		templateUrl: baseUrl + '/views/BookingDetail.html',
		controller: 'BookingDetailController'
	});
	$routeProvider.when(baseUrl + '/bookings/:id', {
		templateUrl: baseUrl + '/views/BookingDetail.html',
		controller: 'BookingDetailController'
	});

	// Users
	$routeProvider.when(baseUrl + '/users', {
		templateUrl: baseUrl + '/views/UserList.html',
		controller: 'UserListController'
	});
	$routeProvider.when(baseUrl + '/users/:id', {
		templateUrl: baseUrl + '/views/UserDetail.html',
		controller: 'UserDetailController'
	});
	
	// Reports
	$routeProvider.when(baseUrl + '/reports', {
		templateUrl: baseUrl + '/views/Reports.html',
		controller: 'ReportsController'
	});
    
    // Settlements
	$routeProvider.when(baseUrl + '/settlements', {
		templateUrl: baseUrl + '/views/SettlementList.html',
		controller: 'SettlementListController'
	});
	$routeProvider.when(baseUrl + '/settlements/add', {
		templateUrl: baseUrl + '/views/SettlementDetail.html',
		controller: 'SettlementDetailController'
	});
	$routeProvider.when(baseUrl + '/settlements/:id', {
		templateUrl: baseUrl + '/views/SettlementDetail.html',
		controller: 'SettlementDetail'
	});

	// Fallback
	$routeProvider.otherwise({
		templateUrl: baseUrl + '/views/404.html'
	});

	$locationProvider.html5Mode(true);
});