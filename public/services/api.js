// From http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
angular.module('crabber').factory('RestfulResource', ['$resource', 'ENV', function($resource, ENV) {
	return function(url, params, methods) {
		var defaults = {
			create: { method: 'post' },
			update: { method: 'put' }
		};

		methods = angular.extend(defaults, methods);

		var resource = $resource(ENV.apiBaseUrl + url, params, methods);
		resource.prototype.$save = function(successCallback, errorCallback) {
			if (!this._id) {
				return this.$create(successCallback, errorCallback);
			} else {
				return this.$update(successCallback, errorCallback);
			}
		};
		return resource;
	};
}]);

/**
 * Provides a booking REST API.
 */
angular.module('crabber').service('Booking', ['RestfulResource', function(RestfulResource) {
	return RestfulResource('/bookings/:id', {
		id: '@_id'
	}, {
		save: { method: 'PUT' }
	});
}]);

angular.module('crabber').service('Settlement', ['RestfulResource', function(RestfulResource) {
	return RestfulResource('/settlements/:id', {
		id: '@_id'
	}, {
		save: { method: 'PUT' }
	});
}]);

/**
 * Provides a user REST API.
 */
angular.module('crabber').service('User', ['RestfulResource', function(RestfulResource) {
	return RestfulResource('/users/:id', {
		id: '@_id'
	});
}]);

/**
 * Provides a group REST API.
 */
angular.module('crabber').service('Group', ['RestfulResource', function(RestfulResource) {
	return RestfulResource('/groups/:id', {
		id: '@_id'
	});
}]);

/**
 * Provides a group REST API.
 */
angular.module('crabber').service('TagShareModificator', ['RestfulResource', function(RestfulResource) {
	return RestfulResource('/tagShareModificators/:id', {
		id: '@_id'
	});
}]);

/**
 * Provides an API which contains all models as dictionary.
 */
angular.module('crabber').service('API', ['Booking', 'User', 'Group', 'TagShareModificator', function(Booking, User, Group, TagShareModificator) {
	return {
		Booking: Booking,
		User: User,
		Group: Group,
		TagShareModificator: TagShareModificator
	};
}]);

angular.module('crabber').factory('authInterceptor', function ($rootScope, $q, $window, $location) {
  return {
    request: function (config) {
      config.headers = config.headers || {};
      if ($window.localStorage.token) {
        config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
	$rootScope.full_name = $window.localStorage.full_name;
	$rootScope._id = $window.localStorage._id;
	$rootScope.login = $window.localStorage.login;
      } else {
	$location.path("/login");
      }
      return config;
    },
    response: function (response) {
      if (response.status === 401) {
        // handle the case where the user is not authenticated
      }
      return response || $q.when(response);
    }
  };
});

angular.module('crabber').config(function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
});