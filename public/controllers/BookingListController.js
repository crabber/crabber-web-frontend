/* global angular, alert, console */

angular.module('crabber').controller('BookingListController', ['$scope', '$routeParams', '$location', '$log', '$timeout', 'Booking', function ($scope, $routeParams, $location, $log, $timeout, Booking) {
	'use strict';
	
	$scope.bookings = Booking.query();

    // 	// STATUS FILTER
 
 	$scope.statusList = [
 		{ key: 'open', label: 'Open' }
 	];
 
 	$scope.statusFilter = function(booking) {
 		if($scope._statusFilter.length === 0) {
            return true;   
        }
        
        if($scope._statusFilter.indexOf('open') !== -1 ) {
            return booking.cleared_by === undefined;
        }
 	};
 
 	$scope._statusFilter = ['open'];
 
 	$scope.setStatusFilter = function(filter) {
 		if (filter === 'all') {
 			$scope._statusFilter = [];
 		} else if ($scope._statusFilter.indexOf(filter) === -1) {
 			$scope._statusFilter.push(filter);
 		} else {
 			$scope._statusFilter.splice($scope._statusFilter.indexOf(filter), 1);
 		}
 	};
 
 	$scope.isStatusFilter = function(filter) {
 		if (filter === 'all') {
 			return $scope._statusFilter.length === 0;
 		} else {
 			return $scope._statusFilter.indexOf(filter) !== -1;
 		}
 	};
 
	// ORDER

	$scope.orderBy = 'booking_date';
	$scope.reverseOrder = false;

	$scope.isOrder = function(orderBy, reverseOrder) {
		return $scope.orderBy === orderBy && $scope.reverseOrder === reverseOrder;
	};
	$scope.order = function(orderBy) {
		if ($scope.orderBy == orderBy) {
			$scope.reverseOrder = !$scope.reverseOrder;
			return;
		}

		// Here we could assign multiple orders later...
		$scope.orderBy = orderBy;
		$scope.reverseOrder = false;
	};
	
	$scope.getTotalAmount = function(booking) {
		var sum = 0;
		
		booking.creditors.forEach(function(creditor) {
			sum += creditor.amount.value;
		});
		
		return sum;
	};

}]);
