/* global angular, alert, console */

angular.module('crabber').controller('SettlementListController', ['$scope', '$routeParams', '$location', '$log', '$timeout', 'Settlement', function ($scope, $routeParams, $location, $log, $timeout, Settlement) {
	'use strict';
	
	$scope.settlements = Settlement.query();


// 	// STATUS FILTER
// 
// 	$scope.statusList = [
// 		{ key: 'DRAFT', label: 'Draft' },
// 		{ key: 'OPEN', label: 'Open' },
// 		{ key: 'ASSIGNED', label: 'Assigned' },
// 		{ key: 'ONTOUR', label: 'On Tour' },
// 		{ key: 'DELIVERED', label: 'Delivered' },
// 		{ key: 'CANCELED', label: 'Canceled' }
// 	];
// 
// 	$scope.statusFilter = function(delivery) {
// 		return $scope._statusFilter.length === 0 || $scope._statusFilter.indexOf(delivery.status) !== -1;
// 	};
// 
// 	$scope._statusFilter = [];
// 
// 	$scope.setStatusFilter = function(filter) {
// 		if (filter === 'all') {
// 			$scope._statusFilter = [];
// 		} else if ($scope._statusFilter.indexOf(filter) === -1) {
// 			$scope._statusFilter.push(filter);
// 		} else {
// 			$scope._statusFilter.splice($scope._statusFilter.indexOf(filter), 1);
// 		}
// 	};
// 
// 	$scope.isStatusFilter = function(filter) {
// 		if (filter === 'all') {
// 			return $scope._statusFilter.length === 0;
// 		} else {
// 			return $scope._statusFilter.indexOf(filter) !== -1;
// 		}
// 	};
// 
// 
	// ORDER

	$scope.orderBy = 'booking_date';
	$scope.reverseOrder = false;

	$scope.isOrder = function(orderBy, reverseOrder) {
		return $scope.orderBy === orderBy && $scope.reverseOrder === reverseOrder;
	};
	$scope.order = function(orderBy) {
		if ($scope.orderBy == orderBy) {
			$scope.reverseOrder = !$scope.reverseOrder;
			return;
		}

		// Here we could assign multiple orders later...
		$scope.orderBy = orderBy;
		$scope.reverseOrder = false;
	};
}]);
