angular.module('crabber').controller('LoginController', ['$rootScope', '$scope', '$http', '$window', '$location', 'ENV', function ($rootScope, $scope, $http, $window, $location, ENV) {
	$scope.user = {username: '', password: ''};
	$scope.message = '';
	$scope.submit = function () {
		
		var requestBody = {
			username: $scope.user.username,
			password: $scope.user.password,
			client: {
				type: 'service-op-web'
			},
			runtime: {
				name: navigator.userAgent
			},
			os: {
				name: navigator.platform
			}
		};
		
		$http
		.post(ENV.apiBaseUrl + '/authenticate', requestBody)
		.success(function (data, status, headers, config) {
			console.log(data.profile);
			$window.localStorage.token = data.token;
			$window.localStorage.full_name = data.profile.full_name;
			$window.localStorage._id = data.profile._id;
			$window.localStorage.login = data.profile.login;
			$window.localStorage.current_group_id = data.profile.groups[0];
			console.log(data);
			$scope.message = 'Welcome';
			$location.path("/");
		})
		.error(function (data, status, headers, config) {
			// Erase the token if the user fails to log in
			delete $window.localStorage.token;
			
			// Handle login errors here
			$scope.message = 'Error: Invalid user or password';
		});
	};
}]);