angular.module('crabber').controller('SettlementDetailController', ['$rootScope', '$scope', '$routeParams', '$http', '$window', '$location', '$filter', 'Settlement', 'User', '$route', 'ENV', function ($rootScope, $scope, $routeParams, $http, $window, $location, $filter, Settlement, User, $route, ENV) {

    function updateBalance() {
        var params = {
            groupId: $window.localStorage.current_group_id,
            userId: $window.localStorage._id,
            start: $scope.settlement.interval_start,
            end: $scope.settlement.interval_end
        };

        $http({
            url: ENV.apiBaseUrl + '/reports/balance',
            method: 'GET',
            params: params
        }).success(function (data, status, headers, config) {

            $scope.amount = data.balance;

        }).error(function (data, status, headers, config) {
            console.error(data);
        });
    }

    $scope.getUserById = function(userId) {
        var foundUser;

        $scope.users.forEach(function (user) {
            if(user._id === userId) {
                foundUser = user;
                return false;
            }
        });

        return foundUser;
    };

    $scope.month_changed = function() {

        $scope.settlement.interval_start = $filter('date')($scope.settlement_month, 'yyyy-MM') + '-01';
        var nextMonth = new Date($scope.settlement_month.getFullYear(), $scope.settlement_month.getMonth()+1, 1);
        $scope.settlement.interval_end = $filter('date')(nextMonth, 'yyyy-MM') + '-01';

        updateBalance();
    };

    $scope.add = function() {

        $scope.settlement.balances = [{
            creditor: $scope.amount >= 0 ? $window.localStorage._id : $scope.users[0]._id,
            debtor: $scope.amount >= 0 ? $scope.users[0]._id : $window.localStorage._id,
            amount: {
                currency: 'EUR',
                value: Math.abs($scope.amount)
            }
        }];

        console.log($scope.settlement);

        $scope.settlement.$save(function(settlement) {
            console.log('Added successfully');
        });
    };


    $scope.settlement = new Settlement();
    $scope.settlement.group = $window.localStorage.current_group_id;

    $scope.settlement_month = new Date();
    $scope.month_changed();

    updateBalance();

    // TODO: only users in active group
    User.query(function(users) {

        $scope.users = users.filter(function(user) {
            return user._id !== $window.localStorage._id;
        });
    });
}]);