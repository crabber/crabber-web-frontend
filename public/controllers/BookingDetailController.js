angular.module('crabber').controller('BookingDetailController', ['$rootScope', '$scope', '$routeParams', '$http', '$window', '$location', 'Booking', 'User', '$route', function ($rootScope, $scope, $routeParams, $http, $window, $location, Booking, User, $route) {

    function updateCreditorsAndDebtors() {
        $scope.booking.debtors = $scope.booking.debtors.map(function(user) {
            return user._id;
        });
        $scope.booking.creditors.forEach(function(creditor) {

            $scope.creditors[creditor.by._id] = {
                value: $scope.amountIntegerToDecimalString(creditor.amount.value),
                currency: creditor.amount.currency
            };
        });
    }

    if($routeParams.id === undefined || $routeParams.id === 'add') {
        $scope.mode = 'add';
        $scope.booking = new Booking();

        $scope.booking.booking_date = Date.now();
        $scope.booking.group =  $window.localStorage.current_group_id;
        $scope.master = angular.copy($scope.booking);
        
    } else {
        $scope.mode = 'edit';
    }

    // TODO: only users in active group
    User.query(function(users) {

        $scope.users = users;

        $scope.creditors = [];

        $scope.users.forEach(function(user) {

            $scope.creditors[user._id] = {
                currency: 'EUR',
                value: 0
            };
        });

        if($scope.mode === 'add') {
            $scope.booking.debtors = $scope.users.map(function(user) {
                return user._id;
            });

        } else {
            // debtors and creditors are populated by backend, replace it with
            // only their references here.
            Booking.get({id: $routeParams.id}, function(booking) { 
                $scope.booking = booking;
                $scope.master = angular.copy($scope.booking);

                updateCreditorsAndDebtors();
            });
        }
    });

    $scope.update = function() {

        var transferBooking = angular.copy($scope.booking);
        transferBooking.creditors = [];

        // filter out empty creditors
        Object.keys($scope.creditors).forEach(function(creditorKey) {
            var creditor = $scope.creditors[creditorKey];
            var value = $scope.amountDecimalStringToInteger(creditor.value);

            if(value !== 0) {
                transferBooking.creditors.push({
                    by: creditorKey,
                    amount: {
                        value: value,
                        currency: creditor.currency
                    }
                });
            }
        });


        transferBooking.$save(function(savedBooking) {
            $scope.booking = savedBooking;
            $scope.master = angular.copy($scope.booking);
            $scope.mode = 'edit';
            updateCreditorsAndDebtors();
        });
    };

    $scope.reset = function() {
        $scope.booking = angular.copy($scope.master);
        updateCreditorsAndDebtors();
    };

    // toggle selection for a given debtor by user _id
    $scope.toggleDebtorSelection = function(userId) {
        var idx = $scope.booking.debtors.indexOf(userId);

        // is currently selected
        if (idx > -1) {
            $scope.booking.debtors.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.booking.debtors.push(userId);
        }
    };

    $scope.getUserById = function(userId) {
        var foundUser;

        $scope.users.forEach(function (user) {
            if(user._id === userId) {
                foundUser = user;
                return false;
            }
        });

        return foundUser;
    };

    $scope.amountIntegerToDecimalString = function amountIntegerToDecimalString(integerValue) {
        return (integerValue * 0.01).toFixed(2).toString().replace(/\./g, ',');
    };

    $scope.amountDecimalStringToInteger = function amountDecimalStringToInteger(decimalString) {

        if(typeof(decimalString) === 'number') {
            decimalString = decimalString.toString();
        }

        return parseInt(decimalString.replace(/,/g, '.') * 100);
    };

    $scope.addAnotherBooking = function() {
        if($location.path() === '/bookings/add') {
            $route.reload();
        } else {
            $location.path("/bookings/add");
        }

    };
}]);