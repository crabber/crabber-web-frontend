angular.module('crabber').controller('LogoutController', ['$rootScope', '$scope', '$http', '$window', '$location', function ($rootScope, $scope, $http, $window, $location) {
	
	delete $window.localStorage.token;
	delete $window.localStorage.full_name;
	delete $window.localStorage._id;
	delete $window.localStorage.login;
	delete $window.localStorage.current_group_id;
}]);