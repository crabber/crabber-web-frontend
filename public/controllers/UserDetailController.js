angular.module('crabber').controller('UserDetailController', ['$rootScope', '$scope', '$routeParams', '$http', '$window', '$location', 'User', 'Group', function ($rootScope, $scope, $routeParams, $http, $window, $location, User, Group) {
	
	$scope.groups = Group.query();
	$scope.roles = { 'user': true, 'sysadmin': false };
	
	if( $routeParams.id === undefined || $routeParams.id === 'add') {
		$scope.mode = 'add';
		$scope.user = new User();
		$scope.user.groups = [];
	} else {
		$scope.mode = 'edit';
		$scope.user = User.get({id: $routeParams.id});
	}
	
	$scope.master = angular.copy($scope.user);
	
	$scope.add = function() {
		
		$scope.update();
	}
	
	$scope.update = function() {
		
		$scope.user.roles = $scope.getActivatedRoles();
		
		$scope.user.$save();
		
		$scope.master = angular.copy($scope.user);
	};
	
	$scope.reset = function() {
		$scope.user = angular.copy($scope.master);
	};
	
	$scope.updatePassword = function() {
		
		$scope.user.password = $scope.newPassword;
		$scope.update();
	}
	
	$scope.resetPasswordField = function() {
		
		$scope.newPassword = '';
	}
	
	// toggle selection for a given group by _id
	$scope.toggleGroupSelection = function(groupId) {
		var idx = $scope.user.groups.indexOf(groupId);
		
		// is currently selected
		if (idx > -1) {
			$scope.user.groups.splice(idx, 1);
		}
		
		// is newly selected
		else {
			$scope.user.groups.push(groupId);
		}
	};
	
	$scope.getGroupById = function(groupId) {
		var foundGroup;
		
		$scope.groups.forEach(function(group) {
			if(group._id === groupId) {
				foundGroup = group;
				return false;
			}
		});
		
		return foundGroup;
	}
	
	$scope.getActivatedRoles = function() {
		
		var activatedRoles = [];
		
		Object.keys($scope.roles).forEach(function(roleName) {
			if($scope.roles[roleName]) {
				activatedRoles.push(roleName);
			}
		});
		
		return activatedRoles;
	}
}])