/* global angular, alert, console */

angular.module('crabber').controller('ReportsController', ['$scope', '$routeParams', '$location', '$log', '$window', '$http', '$filter', 'User', 'ENV', function ($scope, $routeParams, $location, $log, $window, $http, $filter, User, ENV) {
	'use strict';
	
	$scope.reportTags = [undefined, 'Lebensmittel', 'Haushalt', 'Möbel', 'auswärts', 'Fahrtkosten', 'Geschenk'];
	$scope.users = User.query(init);
	$scope.expenses = {};
	$scope.shares = {};
	$scope.balances = {};
	$scope.month = new Date();
	
	var params = {
		groupId: $window.localStorage.current_group_id
	};
	
	$scope.update = function update() {
		
		$scope.users.forEach(function(user) {
			
			$scope.expenses[user._id] = {};
			$scope.shares[user._id] = {};
			$scope.balances[user._id] = {};
			
			fetch(user, 'expenses', $scope.expenses[user._id], 'expenses');
			fetch(user, 'share', $scope.shares[user._id], 'share');
			fetch(user, 'balance', $scope.balances[user._id], 'balance');
		});
	}
	
	$scope.rangeChanged = function rangeChanged() {
		
		$scope.update();
	};
	
	
	$scope.getUserById = function(userId) {
		var foundUser;
		
		$scope.users.forEach(function (user) {
			if(user._id === userId) {
				foundUser = user;
				return false;
			}
		});
		
		return foundUser;
	};
	
	$scope.expensesCheckSumsPerTag = function() {
	
		var sumsPerTag = {};
		
		Object.keys($scope.expenses).forEach(function(userId) {
			
			Object.keys($scope.expenses[userId]).forEach(function(tag) {
				
				if(sumsPerTag[tag] === undefined) {
					sumsPerTag[tag]	= 0;
				}
			
				sumsPerTag[tag] += $scope.expenses[userId][tag];
			});
		});
		
		
		return sumsPerTag;
	};
	
	$scope.sharesCheckSumsPerTag = function() {
	
		var sumsPerTag = {};
		
		Object.keys($scope.shares).forEach(function(userId) {
			
			Object.keys($scope.shares[userId]).forEach(function(tag) {
				
				if(sumsPerTag[tag] === undefined) {
					sumsPerTag[tag]	= 0;
				}
			
				sumsPerTag[tag] += $scope.shares[userId][tag];
			});
		});
		
		
		return sumsPerTag;
	};
	
	function init() {
		
		$scope.update();
	}
	
	function fetch(user, path, values, resultProp) {
		
		var sum = 0;
		
		$scope.reportTags.forEach(function(reportTag) {
			
			var params = {
				groupId: $window.localStorage.current_group_id,
				userId: user._id,
			};
			
            params.start = $filter('date')($scope.month, 'yyyy-MM') + '-01';

            var nextMonth = new Date($scope.month.getFullYear(), $scope.month.getMonth()+1, 1);
            params.end = $filter('date')(nextMonth, 'yyyy-MM') + '-01';
			
			if(reportTag !== undefined) {
				params.tags = reportTag;
			}
			
			console.log(params);
			
			$http({ url: ENV.apiBaseUrl + '/reports/' + path, method: 'GET', params: params})
			.success(function (data, status, headers, config) {
				
				if(reportTag) {
					sum += data[resultProp];
				}
				
				values[reportTag ? reportTag : 'Gesamt'] = data[resultProp];
				
				values['Sonstiges'] = values['Gesamt'] - sum;
			})
			.error(function (data, status, headers, config) {
				console.error(data);
			});
		});
	}
}]);
