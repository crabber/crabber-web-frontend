FROM node:4
MAINTAINER Fabian Köster <koesterreich@fastmail.fm>

# Set timezone to Europe/Berlin
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Set workdir
WORKDIR /src

# Copy over package.json
ADD package.json ./

# Install dependencies
RUN npm install --silent

# Copy over bower.json
ADD bower.json ./

# Bower fails when run as root, so start again with --allow-root
RUN node ./node_modules/.bin/bower --allow-root install

# Copy over remaining sources
ADD . ./

# Start the web application server
CMD ./node_modules/.bin/grunt production && node server.js

# The port(s) the web application uses
EXPOSE 5000
